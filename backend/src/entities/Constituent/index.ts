interface Constituent {
    id: number
    name: string
    email: string
    phone: string
    external_id: string
    created_at: Date
    updated_at: Date
}

export default Constituent