
import { ConstituentRepositoryImpl } from "../../aws/dynamodb";
import { CreateConstituentUseCase } from "../../usecases";
import { ConstituentMapper } from "../mappers";

class ConstituentController {
  private readonly constituentRepository: ConstituentRepositoryImpl

  constructor(
    constituentRepository: ConstituentRepositoryImpl
  ) {
    this.constituentRepository = constituentRepository
  }

  async createUser<T>(req: StandardRequest<T>, res: StandardResponse<T>): Promise<StandardResponse<T>> {
    try {
      const constituent = ConstituentMapper.toDomain(req.body)
      const newConstituent = await new CreateConstituentUseCase(this.constituentRepository).execute(constituent)
      const externalDto = ConstituentMapper.toExternalDto(newConstituent)
      return res.status(201).json(externalDto)
    } catch (err) {
      //Log error 
      return res.status(500).json({ error: 'Internal server error' })
    }
  }
}

export default ConstituentController