
class ConstituentFileRepositoryImpl implements ConstituentFileRepository {
    // todo - this repository would be responsible for uploading files to s3 for the consistuent service
    // in this case a batch upload would go as follows - request should be presigned

    // on the UI we specify the merge fields - like matching email with email, email_2 with email_2, email with email_2, etc.
    // then the file would be uploaded into s3 for processing and one of two things could happen:

    // we could merge the data and call it a day if those matches happen, or create a new constituent and join it against any existing constituent as a possible duplicate.
    // this way if email_1 matches constituent_1 and email_2 matches constituent_2 - we would have these duplicates all joined against each other on the UI for the user to deduplicate

    const s3path = 'url for s3'
    // Todo define types for request/response
    async upload(request, response): Promise<void> {
        const file = request.file
        if(!file){
            return bad request
        }

        const s3Client = new S3Client({})

        // upload s3 file with fields to match on and with data
        // and event can then kick off from the file upload and process the file

        // alerting the user would happen through some sort of polling or socket or some other event system from the server

    }
}

export default ConstituentFileRepositoryImpl