import { Constituent } from "../../../entities";


class ConstituentMapper {
    static toDomain(item: any): Constituent {

        // map as needed
        return {
            ...item
        }
    }

    static ToDynamoDbItem(constituent: Constituent): any {

        // map as needed
        return {
            ...constituent
        }
    }
}

export default ConstituentMapper