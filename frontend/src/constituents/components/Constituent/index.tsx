import { Avatar, Grid } from "@mui/material"
import React from "react"
import { useGetConstituent } from "../../service/hooks"

interface ConstituentProps {
    email: string

}

const ConstituentTemplate: React.FC<ConstituentProps> = ({
}) => {
    // can either call here or in a controller, preferably in an outside 
    // component so that this can be "dumb", but for now hook is here

    const {constituent, loading, error} = useGetConstituent()

    if(loading) {
        return <>Skeletal loader here</>
    }

    if(error) {
        return <>Error component here</>
    }

    if(!constituent) {
        return <>No constituent found component here</>
    }

    return (
        <Grid container>
            <Grid item xs={2}>
                <Avatar />
            </Grid>
            <Grid item xs={10}>
                <>Constituent component here</>
            </Grid>
        </Grid>
    )
}

export default ConstituentTemplate