import { Constituent } from "../../entities";
import { ConstituentRepository } from "../../repositories";


class CreateConstituentUseCase {
    constructor(private readonly constituentRepository: ConstituentRepository) {}

    async execute(constituent: Constituent): Promise<Constituent> {
        // pre creation logic
        const newConstituent = await this.constituentRepository.create(constituent);
        // post creation logic
        // return whatever is required - for now just the constituent
        return newConstituent
      }
}

export default CreateConstituentUseCase