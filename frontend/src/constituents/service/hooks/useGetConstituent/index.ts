import { useEffect, useState } from "react"
import getConstituent, { GetConstituent } from "../../queries/getConstituent"

const useGetConstituent = () => {
    const [constituent, setConstituent] = useState<GetConstituent | undefined>()
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState<string>()

    const mockConstituentId = 1

    useEffect(() => {
        const call = async () => {
            setLoading(true)
            
            try {
                // in the future make axios
                const response = await getConstituent(mockConstituentId)
                // could check here or build some wrapper for no results
                // for now im not
                setConstituent(response)
            } catch (e: unknown) {
                // log error for logger application
                setError("An error occured getting this constituent.")
            }

            setLoading(false)
        }

        call()
    }, [])

    return { constituent, loading, error }
}

export default useGetConstituent