import { Router } from 'express'
import serverless from 'serverless-http'
import { ConstituentController } from '../controllers'
import { ConstituentRepositoryImpl } from '../aws/dynamodb';

const router = Router()

const constituentRepository = new ConstituentRepositoryImpl();
const constituentController = new ConstituentController(constituentRepository);

router.post('/constituents', constituentController.createUser)

export const handler = serverless(router)
// todo problem - detecting multiple handlers