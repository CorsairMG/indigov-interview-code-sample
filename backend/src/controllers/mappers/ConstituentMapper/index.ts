import { Constituent } from "../../../entities";

// generic mapper but would be specific to whatever the external entity might look like
// like SalesForceContactMapper.toDomain() -> converts salesforce contact to internal constituent
class ConstituentMapper {
    // instead of accepting any this would be typed to w/e the external is
    static toDomain(externalDto: any): Constituent {

        // map as needed
        return {
            ...externalDto
        }
    }

    // instead of returning any this would ideally be typed
    static toExternalDto(constituent: Constituent): any {

        // map as needed
        return {
            ...constituent
        }
    }
}

export default ConstituentMapper