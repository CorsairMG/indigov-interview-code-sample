import './App.css';
import { Dashboard } from './common/components';

function App() {
  return (
    <>
      <Dashboard />
    </>
  );
}

export default App;
