interface StandardResponse<T> {
    status(code: number): StandardResponse<T>
    json(data: Record<string, any>): StandardResponse<T>
}