import { Constituent } from "../../entities";

interface ConstituentRepository {
  create(constituent: Constituent): Promise<Constituent>
}

export default ConstituentRepository