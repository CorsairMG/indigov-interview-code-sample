import { DynamoDB } from "aws-sdk";
import { Constituent } from "../../../entities";
import { ConstituentRepository } from "../../../repositories";
import { ConstituentMapper } from "../../mappers";


class ConstituentRepositoryImpl implements ConstituentRepository {
    private readonly dynamodb: DynamoDB.DocumentClient;

    constructor() {
        this.dynamodb = new DynamoDB.DocumentClient();
    }

    async create(constituent: Constituent): Promise<Constituent> {
        const putParams = {
            TableName: 'constituents',
            Item: {
                id: constituent.id,
                name: constituent.name,
            },
        };

        // todo wrap this and log error for monitoring app
        const data = await this.dynamodb.put(putParams).promise();
        
        // cant remember how to extract the item from the resulting data - mapper being here doesnt feel right either
        return ConstituentMapper.toDomain(data.$response.data)
    }
}

export default ConstituentRepositoryImpl